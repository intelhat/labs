
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 -->

 <style>
 	.content {
 		display:none;
 	}
 </style>


<div class="container">
<div class="row">
	<div class="col-md-12">
		<!-- Tabs menu -->
		<ul class="nav nav-tabs">
			<li class="nav-item" rel='0'>
		    	<a class="nav-link active" href="#">Form</a>
		  	</li>
		  	<li class="nav-item" rel='1'>
		    	<a class="nav-link" href="#">Process</a>
		  	</li>
		  	<li class="nav-item" rel='2'>
		    	<a class="nav-link" href="#">Project</a>
		  	</li>
		</ul>			

		<!-- Form -->
		<div style='border:solid 1px #ddd; border-top:none; padding:10px;'>

		<!-- content1 -->
			<div class='content' rel='0'>
			<form method='post'>
				<div class="row">
					<div class="col-md-6">
						<div><span style='font-weight:bold;'>Title:</span> <span>(default is project_ymdhis)</span></div>
						<input type="text" name='title' class='form-control' style='width:100%; margin-bottom:10px;'>

						<div style='font-weight:bold;'>Project: </div>
						<select name="project" style=' margin-bottom:10px;' class='form-control'>
							<option value="iremth">IREM (THAI)</option>
							<option value="iremen">IREM (ENG)</option>
						</select>
					</div>
					<div class="col-md-6">
						<div style='font-weight:bold;'>Path: </div>
						<textarea class='form-control' name="path" style='width:100%;' rows="7"></textarea>
					</div>
					<div class="col-md-12 text-md-right" style='margin-top:10px;'>
						<button type='submit' class='btn btn-md btn-primary'>Confirm</button>	
					</div>
				</div>
			</form>
			<hr>
			<button type="button">Test</button>
			<!-- <div class='alert alert-danger'>testing</div> -->
			</div>

		<!-- content2 -->
			<div class='content' rel='1'>
			222
			</div>

		<!-- content2 -->
			<div class='content' rel='2'>
				<div style="font-weight:bold;">Project list:</div>
				<table class="table table-sm">
					<thead>
						<tr>
							<th style='width:50px;'>#</th>
							<th>Detail</th>
							<th style='width:200px;'>Tools</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td style='font-size:12px;'>
								<div style='margin-bottom:8px;'>
									<div style='font-weight:bold;'>Code:</div>
									<div>irem</div>
								</div>

								<div style='margin-bottom:8px;'>
									<div style='font-weight:bold;'>Directory path:</div>
									<div>ddc/irem</div>
								</div>

								<div style='margin-bottom:8px;'>
									<div style='font-weight:bold;'>Remark:</div>
									<div>ddc.irem ภาษาไทย</div>
								</div>
							</td>
							<td>
								<button type="button" class='btn btn-sm btn-warning'>Edit</button>
								<button type="button" class='btn btn-sm btn-danger'>Remove</button>
							</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>

<?php 
	function createDir($dir = false) {
		if(!$dir) { return false; }
		else {
			$path = '';
			$dirExp = explode('/',$dir);
			foreach($dirExp as $i) {
				$path .= (empty($path)?'':'/').$i;
				if(!is_dir($path)) {
					mkdir($path);
				}
			}
		}
	}


	if(!empty($_POST)) {
		$path = $_POST['path'];
		$pathList = explode('
',$path);

		// Project
		$project = array(
			'iremth'=>'ddc/irem',
			'iremen'=>'ddc/iremen'
		);
		// Title
		$pathTarget = (empty($_POST['title'])?$_POST['project'].'_'.date('ymdhis'):$_POST['title']).'/';
		// path target
		$pathProject = '../../'.$project[$_POST['project']].'/';

		foreach($pathList as $i) {
			$i = trim($i);
			$pathTmp = explode('/',$i);

			$filename = @end($pathTmp);
			unset($pathTmp[count($pathTmp)-1]);
			$dir = implode('/',$pathTmp);

			if(is_file($pathProject.$dir.'/'.$filename)) {
				// copy directory
				if(!is_dir($pathTarget.$dir)) {
					createDir($pathTarget.$dir);
				}
				// copy files
				copy($pathProject.$dir.'/'.$filename, $pathTarget.$dir.'/'.$filename);
			}

		}

	}
?>


<script>
	function tabsSelect(target) {
		// tabs
		$('.nav .nav-item>a.active').removeClass('active');
		$('.nav .nav-item').eq(target).find('a').addClass('active');
		// content
		$('.content').hide();
		$('.content[rel='+target+']').show();
	}
	$(function(){
		// tabs menu
		tabsSelect(2);
		$('.nav .nav-item').on('click', function(){
			tabsSelect($(this).attr('rel'));
		});
	});
</script>